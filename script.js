var sort = [];
var _id = 0;

$('button#add').on('click', function(event){
    update();
});

$('input').keypress(function (e) {
    if (e.which == 13) {
        update();
    }
  });

$('div').on('click', 'span#remove', function(){
    //get array id stored in id element
        var toRemove = $(this).parent().get(0).id;
    //find index in array
        var remove = sort.map(function(item){ return item._id; }).indexOf(toRemove);
    //remove it!
        sort.splice(remove, 1);

        $(this).parent().fadeOut(500, function(){
            $(this).remove();
        });
});

$('button#reset').on('click', function(){
    $('#list').empty();
});

function update(){
    var name = $('input[name=name]').val();
    var roll = $('input[name=roll]').val();
    _id++;

    $('#list').empty();

    sort.push({'name': name, 'roll': roll, 'id': _id});
    if(sort.length > 1){
        sort = sort.sort(function(a, b){
            return parseFloat(b.roll) - parseFloat(a.roll)
        });
    }  
    
    sort.forEach(function(item){
        console.log(item.name);
        $('#list').append('<div class="ui segment green temp" id="' + _id + '">' + item.name + ' - ' + item.roll + ' <span id="remove"><i class="large red minus square icon"></i></span></div>');
    });
};