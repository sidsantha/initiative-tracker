# initiative-tracker

A simple web-based initiative tracker for tabletop RPG's.

[Demo Here](http://tools.thomasdneal.com)

Features:
* Auto Sorting of initiatives.
* Removal from initiative order.


ToDo:
* Add if someone has taken there turn in round.
* Add new round (to reset who has taken there turns).
* Add undo if you accidentally remove someone.
* ~~Make "enter" submit also.~~
